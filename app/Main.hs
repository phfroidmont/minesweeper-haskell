{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE FlexibleInstances  #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeOperators      #-}

module Main where

import           Data.Function   ((&))
import           Minesweeper
import           Options.Generic
import           Polysemy
import           Random
import           Terminal

data Parameters w =
  Parameters
    { width  :: w ::: Int <?> "Width of the board"
    , length :: w ::: Int <?> "Length of the board"
    , bombs  :: w ::: Int <?> "Number of bombs to be placed"
    }
  deriving (Generic)

instance ParseRecord (Parameters Wrapped)

deriving instance Show (Parameters Unwrapped)

main :: IO ()
main = do
  (Parameters boardWidth boardLength bombsCount) <- unwrapRecord "Minesweeper"
  runMinesweeper boardWidth boardLength bombsCount & runTerminalIO & runRandomIO & runM
