{-# LANGUAGE TemplateHaskell #-}

module Terminal where

import Polysemy

data Terminal m a where
  PrintLine :: String -> Terminal m ()
  ReadLine :: Terminal m String

makeSem ''Terminal

runTerminalIO :: Member (Embed IO) r => Sem (Terminal ': r) a -> Sem r a
runTerminalIO =
  interpret $ \case
    PrintLine line -> embed $ putStrLn line
    ReadLine -> embed getLine