{-# LANGUAGE TemplateHaskell #-}

module Random where

import Polysemy
import qualified System.Random as R

data Random m a where
  NewGenerator :: Random m R.StdGen

makeSem ''Random

runRandomIO :: Member (Embed IO) r => Sem (Random ': r) a -> Sem r a
runRandomIO =
  interpret $ \case
    NewGenerator -> embed R.newStdGen