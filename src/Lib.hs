module Lib
  ( createBoard
  , generateRandomCoordinates
  , revealTile
  , convertBoardToString
  , revealAll
  , isGameLost
  , isGameWon
  , Board
  , Tile(..)
  , TileState(..)
  ) where

import           Data.List
import           System.Random

type Board = [[Tile]]

data Tile
  = Value Int TileState
  | Bomb TileState
  deriving (Eq, Show)

data TileState
  = Hidden
  | Revealed
  deriving (Eq, Show)

type Coordinates = (Int, Int)

createBoard :: Int -> Int -> [Coordinates] -> Board
createBoard boardWidth boardLength = foldr placeBomb createClearBoard
  where
    createClearBoard = replicate boardLength createClearLine
    createClearLine = replicate boardWidth (Value 0 Hidden)
    placeBomb (x, y) board = mapAroundTile incrementValueTile (x, y) $ replaceTile board (x, y) (Bomb Hidden)
    incrementValueTile (x, y) board =
      case findTile board (x, y) of
        (Just (Value value Hidden)) -> replaceTile board (x, y) (Value (value + 1) Hidden)
        _ -> board

revealTile :: Coordinates -> Board -> Board
revealTile (x, y) board =
  case findTile board (x, y) of
    (Just tile) -> revealHiddenTile tile
    Nothing     -> board
  where
    revealHiddenTile (Value 0 Hidden) = mapAroundTile revealTile (x, y) $ replaceTile board (x, y) (Value 0 Revealed)
    revealHiddenTile (Value value Hidden) = replaceTile board (x, y) (Value value Revealed)
    revealHiddenTile (Bomb Hidden) = revealAll board
    revealHiddenTile _ = board

revealAll :: Board -> Board
revealAll = map revealLine
  where
    revealLine = map revealSingleTile
    revealSingleTile (Value value Hidden) = Value value Revealed
    revealSingleTile (Bomb Hidden)        = Bomb Revealed
    revealSingleTile tile                 = tile

replaceTile :: Board -> Coordinates -> Tile -> Board
replaceTile board (x, y) tile =
  let (firstRows, line:lastRows) = splitAt y board
      (firstTiles, _:lastTiles) = splitAt x line
   in firstRows ++ (firstTiles ++ tile : lastTiles) : lastRows

findTile :: Board -> Coordinates -> Maybe Tile
findTile board (x, y) =
  if not (coordinatesExists board (x, y))
    then Nothing
    else Just $ board !! y !! x

coordinatesExists :: Board -> Coordinates -> Bool
coordinatesExists board (x, y) = isPositive && isYWithinTheBoard board && isXWithinTheBoard board
  where
    isPositive = x >= 0 && y >= 0
    isYWithinTheBoard = (> y) . length
    isXWithinTheBoard = any ((> x) . length)

convertBoardToString :: Board -> String
convertBoardToString = concatMap printLine
  where
    printLine line = concatMap ((' ' :) . printTile) line ++ "\n"
    printTile (Bomb Hidden)      = "■"
    printTile (Bomb Revealed)    = "✸"
    printTile (Value _ Hidden)   = "■"
    printTile (Value 0 Revealed) = "□"
    printTile (Value x Revealed) = show x

mapAroundTile :: (Coordinates -> Board -> Board) -> Coordinates -> Board -> Board
mapAroundTile f (x, y) =
  f (x - 1, y - 1) .
  f (x, y - 1) . f (x + 1, y - 1) . f (x - 1, y) . f (x + 1, y) . f (x - 1, y + 1) . f (x, y + 1) . f (x + 1, y + 1)

generateRandomCoordinates :: Int -> Int -> Int -> StdGen -> [Coordinates]
generateRandomCoordinates maxX maxY count generator = take count . nub $ zip randomXs randomYs
  where
    (xGen, yGen) = split generator
    randomXs = randomRs (0, maxX) xGen
    randomYs = randomRs (0, maxY) yGen

isGameLost :: Board -> Bool
isGameLost = any isRevealedBomb . concat
  where
    isRevealedBomb :: Tile -> Bool
    isRevealedBomb (Bomb Revealed) = True
    isRevealedBomb _               = False

isGameWon :: Board -> Bool
isGameWon = all isRevealedValueOrHidden . concat
  where
    isRevealedValueOrHidden (Value _ Revealed) = True
    isRevealedValueOrHidden (Bomb Hidden)      = True
    isRevealedValueOrHidden _                  = False
