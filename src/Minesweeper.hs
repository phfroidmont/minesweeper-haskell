module Minesweeper where

import Lib
import Polysemy
import Random
import Terminal
import Text.Read

runMinesweeper ::
     Member Terminal r
  => Member Random r =>
       Int -> Int -> Int -> Sem r ()
runMinesweeper boardWidth boardLength bombsCount = do
  generator <- newGenerator
  gameStep . createBoard boardWidth boardLength $
    generateRandomCoordinates (boardWidth - 1) (boardLength - 1) bombsCount generator

gameStep :: Member Terminal r => Board -> Sem r ()
gameStep board = do
  printLine . convertBoardToString $ board
  coordinates <- runGetCoordinates
  let nextBoard = revealTile coordinates board
  if isGameLost nextBoard
    then runGameLost nextBoard
    else if isGameWon nextBoard
           then runGameWon nextBoard
           else gameStep nextBoard

runGetCoordinates :: Member Terminal r => Sem r (Int, Int)
runGetCoordinates = do
  printLine "Enter the coordinates of the tile to reveal (X then Y, zero indexed):"
  xString <- readLine
  yString <- readLine
  case (readMaybe xString, readMaybe yString) of
    (Just x, Just y) -> return (x, y)
    _ -> printLine "Invalid coordinates!" >> runGetCoordinates

runGameLost :: Member Terminal r => Board -> Sem r ()
runGameLost board = do
  printLine . convertBoardToString . revealAll $ board
  printLine "Boom! You lost!"

runGameWon :: Member Terminal r => Board -> Sem r ()
runGameWon board = do
  printLine . convertBoardToString $ board
  printLine "You won! All the bombs were found!"