import           Lib
import           Test.Tasty
import           Test.Tasty.HUnit

main :: IO ()
main = defaultMain $ testGroup "Minesweeper tests" [revealTileTest, createBoardTest]

hiddenBoard :: Board
hiddenBoard =
  [ [Bomb Hidden, Value 1 Hidden, Value 0 Hidden]
  , [Value 1 Hidden, Value 1 Hidden, Value 0 Hidden]
  , [Value 0 Hidden, Value 0 Hidden, Value 0 Hidden]
  ]

revealedBoard :: Board
revealedBoard =
  [ [Bomb Revealed, Value 1 Revealed, Value 0 Revealed]
  , [Value 1 Revealed, Value 1 Revealed, Value 0 Revealed]
  , [Value 0 Revealed, Value 0 Revealed, Value 0 Revealed]
  ]

revealTileTest :: TestTree
revealTileTest =
  testGroup
    "Reveal tile"
    [ testCase "Empty board" $ revealTile (1, 1) [] @?= []
    , testCase "Reveal bomb tile" $ revealTile (0, 0) hiddenBoard @?= revealedBoard
    , testCase "Flag 0 value tile" $
      revealTile (2, 0) hiddenBoard @?=
      [ [Bomb Hidden, Value 1 Revealed, Value 0 Revealed]
      , [Value 1 Revealed, Value 1 Revealed, Value 0 Revealed]
      , [Value 0 Revealed, Value 0 Revealed, Value 0 Revealed]
      ]
    , testCase "Flag non 0 value tile" $
      revealTile (1, 0) hiddenBoard @?=
      [ [Bomb Hidden, Value 1 Revealed, Value 0 Hidden]
      , [Value 1 Hidden, Value 1 Hidden, Value 0 Hidden]
      , [Value 0 Hidden, Value 0 Hidden, Value 0 Hidden]
      ]
    , testCase "Flag revealed bomb tile" $ revealTile (0, 0) revealedBoard @?= revealedBoard
    , testCase "Flag revealed value tile" $ revealTile (1, 2) revealedBoard @?= revealedBoard
    ]

createBoardTest :: TestTree
createBoardTest =
  testGroup
    "Create board"
    [ testCase "Empty board" $ createBoard 0 0 [] @?= []
    , testCase "3x3 single bomb" $ createBoard 3 3 [(0, 0)] @?= hiddenBoard
    , testCase "3x3 three bombs" $
      createBoard 3 3 [(0, 0), (1, 1), (2, 2)] @?=
      [ [Bomb Hidden, Value 2 Hidden, Value 1 Hidden]
      , [Value 2 Hidden, Bomb Hidden, Value 2 Hidden]
      , [Value 1 Hidden, Value 2 Hidden, Bomb Hidden]
      ]
    , testCase "3x3 four bombs" $
      createBoard 3 3 [(0, 1), (1, 1), (2, 1), (1, 0)] @?=
      [ [Value 3 Hidden, Bomb Hidden, Value 3 Hidden]
      , [Bomb Hidden, Bomb Hidden, Bomb Hidden]
      , [Value 2 Hidden, Value 3 Hidden, Value 2 Hidden]
      ]
    ]
